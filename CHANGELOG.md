## UnReleased

- [**Style**: Task 5](https://app.clickup.com/t/none)

## Release v20220402_1130

- [**Style**: Task 4](https://app.clickup.com/t/2artmte)

- [**Docs**: Task 3](https://app.clickup.com/t/2artmkk)

- [**Fix**: Task 2](https://app.clickup.com/t/2artmka)

- [**Feat**: Task 1](https://app.clickup.com/t/2artmk9)

## Release v20220402_0242

- [**Feat**: Add release data to MR](https://app.clickup.com/t/2arrw1a)

- [**Feat**: Task to deploy urgent](https://app.clickup.com/t/2arrvr4)

## Release v0
